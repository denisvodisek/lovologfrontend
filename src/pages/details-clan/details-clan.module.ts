import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailsClanPage } from './details-clan';

@NgModule({
  imports: [
    IonicPageModule.forChild(DetailsClanPage),
  ],
})
export class DetailsClanPageModule {}
