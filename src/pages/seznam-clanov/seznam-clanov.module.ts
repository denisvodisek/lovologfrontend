import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeznamClanovPage } from './seznam-clanov';

@NgModule({

  imports: [
    IonicPageModule.forChild(SeznamClanovPage),
  ],
})
export class SeznamClanovPageModule {}
