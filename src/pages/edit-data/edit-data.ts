import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {RestDruzinaProvider} from "../../providers/rest-druzina/rest-druzina";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LovskaDruzina} from "../../model/lovskaDruzina";
import {Clan} from "../../model/clan";
import {Storage} from "@ionic/storage";
import {RestAuthProvider} from "../../providers/rest-auth/rest-auth";
import {RestClan} from "../../providers/rest-clan/rest-clan";
import {SettingsPage} from "../settings/settings";

/**
 * Generated class for the EditDataPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-data',
  templateUrl: 'edit-data.html',
})
export class EditDataPage {

    updated: Clan;

    clan:Clan;

    lovskeDruzine: LovskaDruzina[];
    editUserForm: FormGroup;
    constructor(public navCtrl: NavController, public navParams: NavParams,
                public restDruzinaProvider: RestDruzinaProvider, private formBuilder: FormBuilder,
                private storage: Storage, private restClan: RestClan){

        this.clan = this.navParams.get('user');
        this.updated = this.clan;
      this.getLovskeDruzine();

      this.editUserForm = this.formBuilder.group( {

            email:[this.clan.elektronska_posta, Validators.compose([
                Validators.pattern(this.emailPattern),
                Validators.required])
            ],
            name:[this.clan.ime, Validators.compose([
                Validators.required,
                Validators.pattern(this.textPattern),
                Validators.minLength(3)
            ])],
            surname:[this.clan.priimek,  Validators.compose([
                Validators.required,
                Validators.pattern(this.textPattern),
                Validators.minLength(3)
            ])],
            role:[this.clan.vloga, Validators.required],
            phoneNumber:[this.clan.telefonska_stevilka,Validators.compose([
                Validators.required,
                Validators.pattern(this.numberPattern),
                Validators.minLength(6),
                Validators.maxLength(9)
            ])],
            druzina:[this.clan.lovska_druzina_id, Validators.required]
        })
    };

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditDataPage');
  }
    getLovskeDruzine(): void{
        this.restDruzinaProvider.getDruzine()
            .subscribe(lovskeDruzine => this.lovskeDruzine = lovskeDruzine);
    }

    //posljemo podatke forma
    edit() {
        this.updated.elektronska_posta = this.editUserForm.controls['email'].value;
        this.updated.ime = this.editUserForm.controls['name'].value;
        this.updated.priimek = this.editUserForm.controls['surname'].value;
        this.updated.telefonska_stevilka = this.editUserForm.controls['phoneNumber'].value;
        this.updated.vloga = this.editUserForm.controls['role'].value;
        this.updated.lovska_druzina_id = this.editUserForm.controls['druzina'].value;

        this.restClan.updateClan(this.updated);
        this.navCtrl.push(SettingsPage);
    }


    emailPattern = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    textPattern = '[a-zA-Z ]*';
    numberPattern='^[0-9]*$';
}
