import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapPage } from './map';

@NgModule({
  imports: [
    IonicPageModule.forChild(MapPage),
  ],
})
export class MapPageModule {}
